# Log

This file is intended to record the development of your project.

At the end of each session, write down what you have done during the session
and what you plan to do during the next one.
This will help you tracking the evolution of the project and
planning ahead the next steps.

You can use the template below for each session.

## 2021-05-17
### Work done during the session
* read doc "read me"
* read doc "let it snow"

### Work planned for the next session
* Try to understand the "Let it snow" algorithm
* Look at the work of last year

#######################################################

## 2021-05-20
### Travail réalisé pendant la session
* Regardé les codes de l'année précédente :
        Autotelechargement on va voir avec les autres groupes
        Code detection neige on va réutiliser les idées des codes présents pour créer le notre (NDSI)

* Diviser le travail en 3 : 
        Récupération des images (voir avec les autres groupes)
        Codage de let it snow pour les bandes disponibles (10 bandes tif de départ)
        Codage boucle pour traiter toutes les images + approfondissement de let it snow

### Travail plannifié pour la prochaine session
* Chacun continue sa mission

#######################################################

## 2021-05-25
### Travail réalisé pendant la session
* Fini de coder Let it snow, mais il y a un pb de complexité car le code n'arrive pas a traiter l'image entière
* Code Autotelechargement n'avance pas, on est bloqués
* Nous allons télécharger une dizaine d'images afin de réaliser un suivi du manteau avec notre programme LIS

### Travail plannifié pour la prochaine session
* Prise en main de QGis pour découper les images satellitaires
* Telechargement 10 images puis réaliser un suivi
* 
