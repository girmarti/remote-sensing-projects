pip install sentinelsat #Install the santinelsat module
from sentinelsat import SentinelAPI, read_geojson, geojson_to_wkt
from datetime import date

#There are two way to download a scene with this module
#1) The first one downloads a single image and needs its ID before downloading it
#2) The second one makes a query with differents parameters and then dowload all images that correspond

## Connection to the user's account
api = SentinelAPI('lolo38', 'teledetectione3', 'https://scihub.copernicus.eu/dhus') #The program logs in with the account

## Method 1
## download single scene by known product id
api.download('be7552d5-70da-4e28-970e-53bc3ab3bed4','.') #The image is dowloaded in the entered path, '.' means in the same folder as the code

##Method 2
## search by polygon
#The area have to be set into a .geojson file, the program will convert it into a wkt file before using it
footprint = geojson_to_wkt(read_geojson('map2.geojson')) #The geojson file is find at the entered path, 'map2.geojson' means that, the file is in the same folder as the code


## search by time, and Hub query keywords
#The images' information are exctracted with the api.query() function
#The parameters of the query are the date, the satellite used, the cloud cover and the *TGK* is a reference to the size of the images
products = api.query(footprint,
                     date = (date(2021, 3, 29), date(2021, 2, 2)),
                     platformname = 'Sentinel-2',
                     cloudcoverpercentage = (0, 100), filename = "*TGK*")
#The function returns a list of the different images with their information

## download all results from the search
#The following line will download every images that have been collected with the last research
output = api.download_all(products,'.')
#This function returns a list of 3 lists corresponding to the success, failed and targetted images
